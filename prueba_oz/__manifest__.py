# -*- coding: utf-8 -*-

{
    'name': 'Prueba Ozsolutions',
    'version': '1.0.1',
    'category': 'All',
    'summary': 'Modulo de prueba',
    'depends': ['stock','account'],
    'data': [
            'security/ir.model.access.csv',
            'data/mail_template_data.xml',
            'views/stock_picking.xml',
            'views/account_move.xml',
            'views/res_partner.xml',
            'wizard/mail_compose.xml',
            'wizard/account_move_reversal.xml',
            'views/report_stock.xml',

    ],
    'demo': [
    ],
    'installable': True,
    'application': True,
    'assets': {

    },
}
