# -*- coding: utf-8 -*-
from odoo import api,fields,models,_

class Partner(models.Model):
    _inherit = "res.partner"

    invoice_due_count = fields.Integer(compute='_compute_invoice_due', string='Facturas pendientes')
    invoice_due_ids = fields.Many2many('account.move', 'Facturas pendientes', compute='_compute_invoice_due')



    def _compute_invoice_due(self):
        for partner in self:
            invoice_due = self.env['account.move'].search([('state','=','posted'),
                                                           ('move_type','=','out_invoice'),
                                                           ('partner_id','=',partner.id),
                                                           ('amount_residual','!=',0),
                                                           ('invoice_date_due','<',fields.Date.today())])
            partner.invoice_due_count = len(invoice_due)
            partner.invoice_due_ids = invoice_due



    def action_invoice_due(self):
        if self.invoice_due_ids.ids:
            action = self.env['ir.actions.act_window']._for_xml_id('account.action_move_out_invoice_type')
            action["domain"] = [("id", "in", self.invoice_due_ids.ids)]
            return action
        else:
            return