
# -*- coding: utf-8 -*-
from odoo import api, fields, models, _, tools


class StockReport(models.Model):
    _name = 'report.stock'
    _description = 'Reporte de inventario'
    _auto = False

    product_id = fields.Many2one('product.product', string='Producto')
    quantity = fields.Float('Catidad disponible', digits='Cantidad disponible')
    location_id = fields.Many2one('stock.location', string='Ubicacion')
    category_id = fields.Many2one('product.category',related='product_id.categ_id', string='Categoria', readonly=True)
    product_uom_id = fields.Many2one(
        'uom.uom', 'Unidad de medida',
        readonly=True, related='product_id.uom_id')
    company_id = fields.Many2one('res.company')

    def init(self):
        tools.drop_view_if_exists(self.env.cr, self._table)
        self.env.cr.execute('''
            CREATE OR REPLACE VIEW %s AS (
            SELECT 
            MIN(id) AS id,
            product_id,
            company_id,
            sum(quantity) as quantity,
            location_id
            FROM stock_quant WHERE company_id = %s 
            AND quantity > 0
            group by product_id,company_id,location_id
            )''' % (self._table,self.env.company.id)
        )