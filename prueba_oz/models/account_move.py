# -*- coding: utf-8 -*-
from odoo import api,fields,models,_

class AccountMove(models.Model):
    _inherit = "account.move"

    responsible_nc_id = fields.Many2one('res.users', string='Responsable', default=lambda self: self.env.user)


