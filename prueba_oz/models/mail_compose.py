# -*- coding: utf-8 -*-
# Part of Odoo. See LICENSE file for full copyright and licensing details.

from odoo import models
import re
from odoo.exceptions import ValidationError




class MailComposeMessage(models.TransientModel):
    _inherit = 'mail.compose.message'

    def _action_send_mail(self, auto_commit=False):
        if self.model == 'stock.picking':
            self.validate_mail()
            self = self.with_context(mailing_document_based=True)
            self = self.with_context(mail_notify_author=self.env.user.partner_id in self.partner_ids)
        return super(MailComposeMessage, self)._action_send_mail(auto_commit=auto_commit)



    def validate_mail(self):
        for partner in self.partner_ids:
            if partner:
                match = re.match('^[_a-z0-9-]+(\.[_a-z0-9-]+)*@[a-z0-9-]+(\.[a-z0-9-]+)*(\.[a-z]{2,4})$', partner.email)
                if match == None:
                    raise ValidationError('Correo invalido %s' % (partner.email))
            else:
                raise ValidationError(('correo vacio en %s') % (partner.name))

