# -*- coding: utf-8 -*-
from odoo import models, fields, api
from odoo.tools.translate import _
from odoo.exceptions import UserError


class AccountMoveReversal(models.TransientModel):
    _inherit = 'account.move.reversal'

    responsible_id = fields.Many2one('res.users', string='Responsable', default=lambda self: self.env.user, required=True)

    def _prepare_default_reversal(self, move):
        # OVERRIDE
        values = super()._prepare_default_reversal(move)
        values.update({
            'responsible_nc_id': self.responsible_id.id,
        })
        return values